# CP Field Instructions plugin for Craft CMS 3.x

Add helpful instructions to your field layouts.

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require mmikkel/cp-field-instructions

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for CP Field Instructions.

## CP Field Instructions Overview

-Insert text here-

## Configuring CP Field Instructions

-Insert text here-

## Using CP Field Instructions

-Insert text here-

## CP Field Instructions Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Mats Mikkel Rummelhoff](http://vaersaagod.no)
