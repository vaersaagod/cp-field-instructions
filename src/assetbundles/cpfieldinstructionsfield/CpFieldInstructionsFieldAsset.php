<?php
/**
 * CP Field Instructions plugin for Craft CMS 3.x
 *
 * Add helpful instructions to your field layouts.
 *
 * @link      http://vaersaagod.no
 * @copyright Copyright (c) 2018 Mats Mikkel Rummelhoff
 */

namespace mmikkel\cpfieldinstructions\assetbundles\cpfieldinstructionsfieldfield;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Mats Mikkel Rummelhoff
 * @package   CpFieldInstructions
 * @since     1.0.0
 */
class CpFieldInstructionsFieldAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@mmikkel/cpfieldinstructions/assetbundles/cpfieldinstructionsfield/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->css = [
            'css/CpFieldInstructionsField.css',
        ];

        parent::init();
    }
}
