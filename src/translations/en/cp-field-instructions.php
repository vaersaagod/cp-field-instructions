<?php
/**
 * CP Field Instructions plugin for Craft CMS 3.x
 *
 * Add helpful instructions to your field layouts.
 *
 * @link      http://vaersaagod.no
 * @copyright Copyright (c) 2018 Mats Mikkel Rummelhoff
 */

/**
 * @author    Mats Mikkel Rummelhoff
 * @package   CpFieldInstructions
 * @since     1.0.0
 */
return [
    'CP Field Instructions plugin loaded' => 'CP Field Instructions plugin loaded',
];
