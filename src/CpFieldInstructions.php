<?php
/**
 * CP Field Instructions plugin for Craft CMS 3.x
 *
 * Add helpful instructions to your field layouts.
 *
 * @link      http://vaersaagod.no
 * @copyright Copyright (c) 2018 Mats Mikkel Rummelhoff
 */

namespace mmikkel\cpfieldinstructions;

use mmikkel\cpfieldinstructions\fields\CpFieldInstructionsField;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\services\Fields;
use craft\events\RegisterComponentTypesEvent;

use yii\base\Event;

// Icon: Information by Gonzalo Bravo from the Noun Project

/**
 * Class CpFieldInstructions
 *
 * @author    Mats Mikkel Rummelhoff
 * @package   CpFieldInstructions
 * @since     1.0.0
 *
 */
class CpFieldInstructions extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var CpFieldInstructions
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = CpFieldInstructionsField::class;
            }
        );

        Craft::info(
            Craft::t(
                'cp-field-instructions',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
