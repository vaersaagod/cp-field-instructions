<?php
/**
 * CP Field Instructions plugin for Craft CMS 3.x
 *
 * Add helpful instructions to your field layouts.
 *
 * @link      http://vaersaagod.no
 * @copyright Copyright (c) 2018 Mats Mikkel Rummelhoff
 */

namespace mmikkel\cpfieldinstructions\fields;

use mmikkel\cpfieldinstructions\CpFieldInstructions;
use mmikkel\cpfieldinstructions\assetbundles\cpfieldinstructionsfieldfield\CpFieldInstructionsFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\helpers\Db;
use yii\db\Schema;
use craft\helpers\Json;

/**
 * @author    Mats Mikkel Rummelhoff
 * @package   CpFieldInstructions
 * @since     1.0.0
 */
class CpFieldInstructionsField extends Field
{
    // Public Properties
    // =========================================================================

    // Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('cp-field-instructions', 'Field Instructions');
    }

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function hasContentColumn(): bool
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getInputHtml($value, ElementInterface $element = null): string
    {
        // Register our asset bundle
        Craft::$app->getView()->registerAssetBundle(CpFieldInstructionsFieldAsset::class);

        return '';
    }
}
